package project

import (
    "gitlab.com/tsuchina.ga/waaw/workload-manager/db"
    "gopkg.in/mgo.v2/bson"
)

const (
    DB = "workload"
    C = "project"
)

func FindId(id string) (project Project, err error) {
    s := db.NewSession()
    defer s.Close()

    err = s.DB(DB).C(C).FindId(bson.ObjectIdHex(id)).One(&project)
    return
}

func FindAll() (projects []Project, err error) {
    s := db.NewSession()
    defer s.Close()

    err = s.DB(DB).C(C).Find(bson.M{}).All(&projects)
    return
}

func Insert(project Project) (err error) {
    s := db.NewSession()
    defer s.Close()

    err = s.DB(DB).C(C).Insert(project)
    return
}

func Update(id string, project Project) (err error) {
    s := db.NewSession()
    defer s.Close()

    err = s.DB(DB).C(C).UpdateId(bson.ObjectIdHex(id), project)
    return
}

func Delete(id string) (err error) {
    s := db.NewSession()
    defer s.Close()

    err = s.DB(DB).C(C).RemoveId(bson.ObjectIdHex(id))
    return
}
