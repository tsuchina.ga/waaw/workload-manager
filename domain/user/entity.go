package user

import "gopkg.in/mgo.v2/bson"

type User struct {
	Id       bson.ObjectId `bson:"_id" json:"id"`
	Name     string        `bson:"name" json:"name" validate:"required"`
	LoginId  string        `bson:"login_id" json:"login_id" validate:"required"`
	Password string        `bson:"password" json:"password" validate:"required"`
}
