package project

import (
	"gopkg.in/mgo.v2/bson"
)

type Project struct {
	Id   bson.ObjectId `bson:"_id" json:"id" form:"-"`
	Name string        `bson:"name" json:"name" form:"name" validate:"required"`
}
