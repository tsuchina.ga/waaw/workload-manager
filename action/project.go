package action

import (
    "github.com/gin-gonic/gin"
    "gitlab.com/tsuchina.ga/waaw/workload-manager/domain/project"
    "gopkg.in/mgo.v2/bson"
)

func ProjectList(c *gin.Context) {
    if ps, err := project.FindAll(); err != nil {
        c.JSON(500, gin.H{"error": err.Error()})
    } else {
        c.HTML(200, "project", gin.H{"title": "Project", "projects": ps})
    }
}

func ProjectAdd(c *gin.Context) {
     var p project.Project
     c.Bind(&p)
     p.Id = bson.NewObjectId()

     if err := project.Insert(p); err != nil {
         c.JSON(500, gin.H{"error": err.Error()})
     } else {
         ProjectList(c)
     }
}
