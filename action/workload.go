package action

import "github.com/gin-gonic/gin"

func WorkloadList(c *gin.Context) {
    c.HTML(200, "workload", gin.H{"title": "Workload"})
}
