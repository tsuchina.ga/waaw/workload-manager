package work

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Work struct {
	Id          bson.ObjectId `bson:"_id" json:"id"`
	Name        string        `bson:"name" json:"name" validate:"required"`
	Minute      int           `bson:"minute" json:"minute" validate:"required"`
	ProjectId   bson.ObjectId `bson:"project_id" json:"project_id"  validate:"required"`
	ProjectName string        `bson:"project_name" json:"project_name"`
	CreatedAt   time.Time     `bson:"created_at" json:"created_at"`
}
