# _WAAW03_ workload-manager

毎週1つWebアプリを作ろうの3回目。

期間は18/05/25 - 18/05/31


## ゴール

* [x] mongodbをgolangから使う
* [ ] Create, Read, Update, Deleteが行なえる
* [ ] 集計が行なえる


## 目的

* [ ] NoSQLの学習
* [ ] 非正規化など含むスキーマの設計
* [x] SPAでないWebページ作成


## 課題

* [x] デザイン(cssテンプレート)

    * [Bulma: a modern CSS framework based on Flexbox](https://bulma.io/)

* [x] templateエンジン

    * [golang で html/template でのテンプレートの継承と、HTML エスケープしないで変数を出力する方法 (Django, Jinja みたいに) - Qiita](https://qiita.com/peketamin/items/1b9b5c74fdb38d52ad77)

* [x] NoFramework -> 結局ginを使うことに

    * [go-chi/chi: lightweight, idiomatic and composable router for building Go HTTP services](https://github.com/go-chi/chi)
    * [gin-gonic/gin: Gin is a HTTP web framework written in Go (Golang). It features a Martini-like API with much better performance -- up to 40 times faster. If you need smashing performance, get yourself some Gin.](https://github.com/gin-gonic/gin)


## 振り返り

0. Idea:

    アイデアやテーマについて
    * テーマはgolang + mgoでwebシステム開発ということで悪くはなかった
    * 作るものも個人的に実用的なものなのでイメージはしっかりできていた
    * golang + mgoを学ぶためのものなのに、templateやcssに振り回された


0. What went right:

    成功したこと・できたこと
    * domainレイヤーを層ではなくて機能で分けてみたら案外使いやすかった
    * ginフレームワークが思いのほか使いやすかった
    * templateの埋め込みも用意


0. What went wrong:

    失敗したこと・できなかったこと
    * そもそも完成してない


0. What I learned:

    学べたこと
    * mngodb + docker周りが一番学べた気がする…


## サンプル

[tcna.ga:9000](http://tcna.ga:9000/)
