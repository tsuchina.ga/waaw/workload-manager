package template

import (
    "html/template"
    "net/http"
    "log"
)

func Execute(w http.ResponseWriter, t string, data map[string]interface{}) {

    funcMap := template.FuncMap{}

    var templateFiles []string
    templateFiles = append(templateFiles, "template/master/master.html")
    templateFiles = append(templateFiles, "template/master/menu.html")
    templateFiles = append(templateFiles, t)

    html := template.Must(template.New("").Funcs(funcMap).ParseFiles(templateFiles...))
    if err := html.ExecuteTemplate(w, "master", data); err != nil {
        log.Println(err)
        http.Error(w, err.Error(), http.StatusInternalServerError)
    }
}
