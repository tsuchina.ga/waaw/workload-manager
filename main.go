package main

import (
    "github.com/gin-gonic/gin"
    "github.com/foolin/gin-template"
    "github.com/gin-gonic/contrib/sessions"
    "time"
    "html/template"
    "gitlab.com/tsuchina.ga/waaw/workload-manager/action"
)

func init() {

    // locationの設定
    const location = "Asia/Tokyo"
    loc, err := time.LoadLocation(location)
    if err != nil {
        loc = time.FixedZone(location, 9*60*60)
    }
    time.Local = loc

}

func main() {
    // ルーティング
    r := gin.Default()

    // middleware
    r.Use(sessions.Sessions("session", sessions.NewCookieStore([]byte("workload-secret"))))

    // static files and templates
    r.Static("/css", "./template/static/css")
    r.Static("/js", "./template/static/js")
    r.HTMLRender = gintemplate.New(gintemplate.TemplateConfig{
        Root: "template",
        Extension: ".html",
        Master: "include/master",
        Funcs: template.FuncMap{
            "add": func(a, b int) int { return a + b },
            "sub": func(a, b int) int { return a - b },
        },
        DisableCache: false,
    })

    // routing
    r.GET("/", action.ProjectList)
    r.GET("/project", action.ProjectList)
    r.POST("/project", action.ProjectAdd)
    r.GET("/workload", action.WorkloadList)

    r.Run(":9000")
}
